package main

import (
	"fmt"
	"strconv"
	"syscall/js"
)

var add js.Func
var subtract js.Func

func initCallbacks() {
	add = js.FuncOf(func(this js.Value, i []js.Value) any {
		value1 := js.Global().Get("document").Call("getElementById", i[0].String()).Get("value").String()
		value2 := js.Global().Get("document").Call("getElementById", i[1].String()).Get("value").String()
		resultNode := js.Global().Get("document").Call("getElementById", i[2].String())

		int1, _ := strconv.Atoi(value1)
		int2, _ := strconv.Atoi(value2)

		resultNode.Set("textContent", fmt.Sprintf("%d", int1+int2))
		return nil
	})
	subtract = js.FuncOf(func(this js.Value, i []js.Value) any {
		value1 := js.Global().Get("document").Call("getElementById", i[0].String()).Get("value").String()
		value2 := js.Global().Get("document").Call("getElementById", i[1].String()).Get("value").String()
		resultNode := js.Global().Get("document").Call("getElementById", i[2].String())

		int1, _ := strconv.Atoi(value1)
		int2, _ := strconv.Atoi(value2)

		resultNode.Set("textContent", fmt.Sprintf("%d", int1-int2))
		return nil
	})
}

func RegisterCallbacks() {
	initCallbacks()
	js.Global().Set("add", add)
	js.Global().Set("subtract", subtract)
	fmt.Println("WASM Go Initialized2")
}

func main() {
	c := make(chan struct{}, 0)

	fmt.Println("WASM Go Initialized")
	// register functions
	RegisterCallbacks()
	<-c
}
