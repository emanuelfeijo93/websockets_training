client:
	cp ~/go/misc/wasm/wasm_exec.js ./cli/webassembly/
	GOARCH=wasm GOOS=js go build -o ./cli/webassembly/lib.wasm ./cli/webassembly/main.go

server:
	go run ./src/main.go -dir ./cli/webassembly/
	