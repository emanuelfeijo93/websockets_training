package main

import (
	"flag"
	"log"
	"net/http"
	"wasmsockets/internal/server"
)

var addr = flag.String("addr", ":8080", "http service address")
var dir = flag.String("dir", ".", "dir to serve")

func main() {
	flag.Parse()
	hub := server.NewHub()
	go hub.Run()
	http.Handle("/", http.FileServer(http.Dir(*dir)))
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		server.ServeWs(hub, w, r)
	})
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
